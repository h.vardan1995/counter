import React, { useState } from "react";

import { Button, Text } from "../components";

import Styles from "./counter.module.scss";

const Counter = () => {
  const [value, setValue] = useState(0);
  return (
    <div>
      <Text title="Counter" />
      <div>{value}</div>

      <div className={Styles.CounterWrapper}>
        <Button
          value="-"
          buttonTitle="Decrease"
          handleClick={() => {
            setValue(value - 1);
          }}
        />
        <Button
          value="+"
          buttonTitle="Increase"
          handleClick={() => {
            setValue(value + 1);
          }}
        />
      </div>
    </div>
  );
};

export default Counter;
