import React from "react";

import { Text } from "./";

const Button = ({ value, buttonTitle, handleClick }) => {
  return (
    <div>
      <div>
        <Text title={buttonTitle} />
      </div>
      <button onClick={handleClick}>{value}</button>
    </div>
  );
};

export default Button;
